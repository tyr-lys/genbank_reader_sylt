package io;

import org.apache.commons.cli.*;

public class CliParser {
    private static Options option;

    public static void main(String[] args) {
        CliParser parser = new CliParser(args);
    }
    private CliParser(final String[] args) {
        makeOptions();
        processOptions(args);
    }

    private static void makeOptions() {
        option = new Options();
        OptionGroup helpRunGroup = new OptionGroup();

        // The user must make a choice between help or running the program,
        // if the input is incorrect a help message is printed
        helpRunGroup.addOption(
                Option.builder("h")
                        .longOpt("help")
                        .desc("Prints this message and the available options for " +
                                "the program, when this option overrules al other options")
                        .build()
        );
        helpRunGroup.addOption(
                Option.builder("i")
                        .longOpt("infile")
                        .desc("Input file from a directory")
                        .hasArg(true)
                        .build()
        );
        helpRunGroup.setRequired(true);

        option.addOption("su","summary",false,"Makes a summary of the parsed genbank file" +
                "and calculates for the genes the forward/reverse balance");
        option.addOption("fg","fetch_gene",true,"Returns nucleotide sequences of" +
                " the genes that match the gene name pattern, in Fasta format");
        option.addOption("fc","fetch_cds",true,"Returns the amino acid sequences of" +
                " the CDSs that match the product name pattern, in Fasta format");
        option.addOption("ff","fetch_features",true,"Returns all models with" +
                " name, type, start, stop and orientation between the given" +
                " coordinates. Coordinates are given as from to and only fully covered models are returned");
        option.addOption("fs","fetch_sites",true,"Lists the locations of all the" +
                " sites where the DNA pattern is found: position and actual" +
                " sequence and the gene in which it resides.");
    }

    private static void processOptions(String [] args) {
//        System.out.println(option);
        try {
            CommandLineParser parser = new DefaultParser();
            parser.parse(option, args);
        } catch (ParseException e) {
            callHelp();
        }

    }

     private static void callHelp() {
        HelpFormatter helpMessage = new HelpFormatter();
        helpMessage.printHelp("java -jar GenbankReader.jar", option);


    }
}
