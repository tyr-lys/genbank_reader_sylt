package io;


import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.lang.String;
import java.util.ArrayList;


public class LineFilter {
//    Starting tags for each operation


     static ArrayList<String> lineFiltering(String inputString, Pattern regexFieldCommentsPattern,
                                            ArrayList<String> lineCollection) {

        Matcher regexFieldCommentsMatcher = regexFieldCommentsPattern.matcher(inputString);
        boolean isBusy;

//      Keeps track to see if it is busy with a tag or not, simplified if statement
        isBusy = lineCollection.size() > 0;

//      Find the tags and their lines
        if (regexFieldCommentsMatcher.find()) {

//          Matches the comment line main tag / initializes a main tag
            if (regexFieldCommentsMatcher.group(1) != null){
//              Not working on a main tag
                if (!isBusy) {
                    lineCollection.add(inputString);

//              Working on a main tag, dump the last one and start a new one
                } else{
                    arrayListDump(lineCollection);
                    lineCollection.clear();
                    if (inputString.contains("FEATURES")) {
                        //TODO do nothing now
                    } else {
                        lineCollection.add(inputString);
                    }

                }
//          Finds up following lines of a field comment line
            } else if (regexFieldCommentsMatcher.group(2) != null){
//                Removes the extra whitespace characters from the beginning of a line
                lineCollection.add(inputString.replaceAll("^ +", "").replaceAll(" +$", ""));
            }else{
                System.out.println("error kip");
            }
        } else{

        }
         System.out.println(Arrays.toString(lineCollection.toArray()));
        return lineCollection;
        }
    private static void arrayListDump (ArrayList<String> lineCollection){
            String concatenatedString = String.join(" ",lineCollection);
            System.out.println(concatenatedString);
    }
    }

