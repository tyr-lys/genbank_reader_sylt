package io;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

/*
The source for the file reader is available at:
https://www.roseindia.net/java/beginners/java-read-file-line-by-line.shtml
*/

public class LineProcessor {

    public static void main(String[] args) {
        String newargs = Arrays.toString(args);
        newargs = newargs.substring(newargs.indexOf("[") + 1, newargs.indexOf("]"));
        LineProcessor lr = new LineProcessor();
        lr.turnFileIntoStream(newargs);
    }

    private void turnFileIntoStream(String genBankFile) {
        try {
            FileInputStream fileStream = new FileInputStream(genBankFile);
            DataInputStream fileInputStream = new DataInputStream(fileStream);
            BufferedReader bufferedFileStreamReader = new BufferedReader( new InputStreamReader(fileInputStream));
            lineProcessing(bufferedFileStreamReader);
//            System.out.println(bufferedFileStreamReader.getClass().getName());
        } catch(FileNotFoundException e){
            System.out.printf("File or Path to %s is not found",genBankFile);
        }

    }
    private void lineProcessing(BufferedReader fileReader){
        String fieldCommentsString = "(^ {0,3}[A-Z]{4,10})|(^ {12}[\\w\\(\\)\\[\\]]\\S)";
        Pattern fieldCommentsPattern = Pattern.compile(fieldCommentsString);
        Matcher fieldCommentsMatcher = null;
                String fileLine;
        int miep = 0;
        ArrayList<String> lineCollection = new ArrayList<String>();
        try {
            while ((fileLine = fileReader.readLine()) != null || miep == 28) {
//                System.out.println(1);
               lineCollection = LineFilter.lineFiltering(fileLine,fieldCommentsPattern, lineCollection);
//                System.out.println(lineCollection.toString());
                miep++;
            }
            fileReader.close();
        }catch (java.io.IOException e){
            System.out.println("Error in this file?");
        }

    }
    private String getFilenameFromPath(String fullPath){
        /* Gets filename from specified path
        * */
        String [] storedPath = fullPath.split("/");
        String fileName = storedPath[storedPath.length-1];
        return fileName;
    }
}

