package models;

public class Summary {
    private String fileName;
    private String organismName;
    private String accessionNumber;
    private int sequenceLength;
    private int numberOfGenes;
    private float geneFRBalance;
    private int numberOfCDS;

    public Summary(String fileName, String organismName, String accessionNumber,
                   int sequenceLength, int numberOfGenes, float geneFRBalance, int numberOfCDS){
        /*
        * Construct new Summary object
        * */
        this.fileName = fileName;
        this.organismName = organismName;
        this.accessionNumber = accessionNumber;
        this.sequenceLength = sequenceLength;
        this.numberOfGenes = numberOfGenes;
        this.geneFRBalance = geneFRBalance;
        this.numberOfCDS = numberOfCDS;
    }

    @Override
    public String toString(){
        /*
        * Makes the the Summary object printable in string form.
        * */
        return
                fileName + "\n"
                + organismName + "\n"
                + accessionNumber + "\n"
                + sequenceLength+ "\n"
                + numberOfGenes + "\n"
                + geneFRBalance + "\n"
                + numberOfCDS + "\n";
    }
}
